const xekSdk = require('xek-sdk');
const g8cSdk = require('@namnh270/g8c-sdk-test');
const config = require('./config');
const sdk = new g8cSdk.SDK(config.chainID, config.walletApiUrl);
const contract = new g8cSdk.Contract(config.chainID, config.walletApiUrl, config.jsonInterface, config.byteCode);
// const contract = new xekSdk.Contract(config.chainID, config.walletApiUrl, config.jsonInterface, config.byteCode);
// const g8cContract = new
const maxSteps = 10;

const deploy = async () => {
  const deploy = await contract.deploy(config.owner1, config.passphraseOwner1, [Number(maxSteps)]);
  console.log(deploy);
};

const saveGame = async addressContract => {
  // Storage a game result
  const saveGameData = await contract.getMethod('SaveGameResultToSM', [
    [
      [1, 4, true],
      [3, 5, true]
    ],
    ['22123BA07C0E8F4972CED6AA80BDD6C7F52E52D9', '22123BA07C0E8F4972CED6AA80BDD6C7F52E52D9']
  ]);
  const saveGame = await contract.broadcastBurrowTx(
    'SaveGameResultToSM',
    config.owner1,
    addressContract,
    config.passphraseOwner1,
    saveGameData
  );
  console.log(`Save game return:`, saveGame);
};

const dissect = async addressContract => {
  // Call page totals
  const getGamedata = await contract.getMethod('getGames', ['5EA795177A6CB191C79599F25A0C468161D9943C', 1]);
  const getGame = await contract.broadcastBurrowTx('getGames', null, addressContract, null, getGamedata);
  console.log(`Get game:`, getGame);

  // Call page totals
  const totalPageData1 = await contract.getMethod('pageTotals', ['5EA795177A6CB191C79599F25A0C468161D9943C']);
  const totalPage1 = await contract.broadcastBurrowTx('pageTotals', null, addressContract, null, totalPageData1);
  console.log(`Total pages:`, totalPage1);
};

// sendtx();
// deploy();
// saveGame('41B4BB414B0C2C89C8F52B8542AAE475BD8064B2');
dissect('0A5A7BD8BF594D3743DB8F5B06B5AAD91B10C9EB');
